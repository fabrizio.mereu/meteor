from flask import Flask, render_template, request, jsonify
import functions


def create_app():
    app = Flask(__name__)

    app.secret_key = b'\x1a\x87\x93kT\xf0\x0b6;\x93\xfdp\x83Ll\xe6u\xa9\xf7x'  # random bytes really

    @app.route('/meteor')
    def main_page():
        return render_template('meteor.html')

    @app.route('/encode', methods=['POST', 'GET'])
    def encode():
        if request.method == 'GET':
            return
        form = request.form.to_dict()
        encoded = functions.encode_message(form['text_to_encode'], form['context']+'\n\n',
                                           bytes(form['initkey'], encoding='utf-8'), b'\x00' * 16,
                                           int(form['precision']))
        doublecheck = functions.decode_message(encoded[0], form['context']+'\n\n',
                                               bytes(form['initkey'], encoding='utf-8'), b'\x00' * 16,
                                               int(form['precision']), encoded[1])
        if doublecheck[0] == 'error':
            return jsonify(message='An error occurred'), 500
        else:
            return jsonify(message=encoded[0]), 200

    @app.route('/decode', methods=['POST', 'GET'])
    def decode():
        if request.method == 'GET':
            return
        form = request.form.to_dict()
        worked, result = functions.decode_message(form['text_to_decode'], form['context'] + '\n\n',
                                               bytes(form['initkey'], encoding='utf-8'), b'\x00' * 16,
                                               int(form['precision']), '')
        if worked == 'error':
            return jsonify(message='An error occurred'), 500
        else:
            return jsonify(message=result), 200
    return app
    
    


if __name__ == "__main__":
    create_app().run(host='0.0.0.0', port=80)
