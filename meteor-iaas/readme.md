# Meteor

https://meteorfrom.space/

Meteor is a cryptographically secure steganography for realistic distributions. The idea is to hide a written
message behind the entropy of a computer generated cleartext; a reader, assuming the same starting parameters
are in common, will later regenerate the same text; by reading the choices made by the generator, they are
able to extract the hidden message. The starting parameters may also be considered some sort of private
symmetric key for the communication system.

Unfortunately, there are problems.
* While Meteor is secure ([read here](https://eprint.iacr.org/2021/686)), it is dependent on the generative model.
A censor will not be able to read the stegotext, but it might identify the covertext as being a product of a generative
model and block it. GPT-2 (used in the implementation of the internet demo), even if being a good model, 
occasionally produce nonsense text.
* Meteor encodes small stegotext over long covertext. The storage for our information is the entropy of choosing one
word over the others, given the previous words. Some series of words produce little entropy, forcing the algorithm
into producing more text, in the hope of producing more entropy later on. For example, after a list of eleven different
months, we can really only select the twelfth month as the next reasonable word.

## Extending Meteor to a StarFall

The underlying system is left untouched; we don't want to reinvent the wheel.

However, we wish to export the idea to other data format, like images, videos, audios or even more, disseminating
much more data in a much larger space.

So far, tentative code exists to load an arbitrary file, but dealing with arbitrary data format is not yet done.

## Objectives completed

* Recreating the results
* Exporting the coder and decoder to a machine-independent system
* Extend the range of possible generative models

## Objectives yet to be achieved

* Arbitrary data encrytpion
* Different kind of generator; right now we can only produce text
* Bypass the occasional desychronization between coding and decoding
